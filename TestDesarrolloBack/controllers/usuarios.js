"use strict";

var mysql = require('mysql');

//local mysql db connection
 var connection = mysql.createConnection({
     host     : 'localhost',
     user     : 'root',
     password : '',
     database : 'usuarios',
     port: '3306'
 });

 connection.connect((err)=>{
     if(!err){
         console.log('BD conectada');
     }else{
         console.log(err);
     }
 })

async function getUsuarios(req, res) {
    let $query = 'SELECT * from usuarios';

    connection.query($query, function(err, rows, fields) {
        if(err){
            console.log("An error ocurred performing the query.");
            return;
        }
    
        res.send(rows);
    });
}

async function getOneUser(req, res) {
    console.log(req.params);
    let $query = 'SELECT * from usuarios WHERE id = ?';

    connection.query($query, [req.params.id], function(err, rows, fields) {
        if(err){
            console.log("An error ocurred performing the query.");
            return;
        }
    
        res.send(rows);
    });
}

async function postUsuarios(req, res) {
    
    delete req.body['id'];
    console.log('hola', req.body);
    let $query = 'INSERT INTO usuarios set ?';

    connection.query($query, [req.body], function(err, rows, fields) {
        if(err){
            console.log("An error ocurred performing the query.");
            return;
        }
    
        res.send(rows);
    });
}

async function updateUsuario(req, res) {
    
    console.log('hola', req.body);
    console.log('hola', req.params);
    let $query = 'UPDATE usuarios SET ? WHERE id = ?';

    connection.query($query, [req.body, req.params.id], function(err, rows, fields) {
        if(err){
            console.log("An error ocurred performing the query.");
            return;
        }
    
        res.send(rows);
    });
}

async function deleteUsuario(req, res) {

    console.log(req.params.id);
    let $query = 'DELETE FROM usuarios WHERE id = ?';

    connection.query($query, [req.params.id], function(err, rows, fields) {
        if(err){
            console.log("An error ocurred performing the query.");
            return;
        }
    
        res.send(rows);
    });
}

module.exports = {
    getUsuarios,
    postUsuarios,
    getOneUser,
    updateUsuario,
    deleteUsuario
};