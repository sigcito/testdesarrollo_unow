var express = require('express');
var router = express.Router();
var Controller = require("../controllers/usuarios");
var api = express.Router();

api.get("/usuarios", Controller.getUsuarios);
api.get("/usuarios/:id", Controller.getOneUser);
api.put("/usuarios/:id", Controller.updateUsuario);
api.post("/usuarios/", Controller.postUsuarios);
api.delete("/usuarios/:id", Controller.deleteUsuario);

module.exports = api;

