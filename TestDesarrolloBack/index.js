'use strict'

var app = require('./app');
/* Entorno pruebas*/
//var port = process.env.PORT || 3500;

/* Entorno DGO */
var port = process.env.PORT || 3600;

app.listen(port, function() {
    console.log('Servidor funcionando ' + port);
});