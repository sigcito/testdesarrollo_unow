function replaceAll(string, search, replace) {
    if (string) { return string.split(search).join(replace); }
}

module.exports = {
    replaceAll
};