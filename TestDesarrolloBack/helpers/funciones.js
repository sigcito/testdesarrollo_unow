function getDiaSemana(fecha) {
    let diaSemana = fecha.getDay() - 1;
    if (diaSemana < 0) {
        diaSemana = 6;
    }
    return diaSemana;
}

function getSemanaDeMes(fecha) {
    let fechaInicioMes = fecha.getFullYear().toString() + '/' + (fecha.getMonth() + 1).toString().padStart(2, '0') + '/01';
    fechaInicioMes = new Date(fechaInicioMes);

    let diaInicioMes = getDiaSemana(fechaInicioMes);
    let diaRealSemana = fecha.getDate() + diaInicioMes;
    let numeroSemana = Math.trunc(diaRealSemana / 7) + 1;
    return numeroSemana;
}

function getNombreMes(mes) {
    let nombre = '';
    switch (mes) {
        case 1:
            nombre = 'Enero';
            break;
        case 2:
            nombre = 'Febrero';
            break;
        case 3:
            nombre = 'Marzo';
            break;
        case 4:
            nombre = 'Abril';
            break;
        case 5:
            nombre = 'Mayo';
            break;
        case 6:
            nombre = 'Junio';
            break;
        case 7:
            nombre = 'Julio';
            break;
        case 8:
            nombre = 'Agosto';
            break;
        case 9:
            nombre = 'Septiembre';
            break;
        case 10:
            nombre = 'Octubre';
            break;
        case 11:
            nombre = 'Noviembre';
            break;
        case 12:
            nombre = 'Diciembre';
            break;
    }
    return nombre;
}


function formattedDate(d = newDate) {
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    const year = String(d.getFullYear());

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return `${day}/${month}/${year}`;
}

function convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

function toDate(dateStr) {
    var parts = dateStr.split("/")
    return new Date(parts[2], parts[1] - 1, parts[0])
}

function blobToFile(theBlob, fileName) {
    //A Blob is almost a File - it's just missing the two properties below
    var f = theBlob;
    f.lastModifiedDate = new Date();
    f.name = fileName;
    return f;
}

module.exports = {
    getSemanaDeMes,
    getNombreMes,
    formattedDate,
    convertDate,
    toDate,
    blobToFile
}