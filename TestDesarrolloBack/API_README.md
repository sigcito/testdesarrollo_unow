El punto de entrada para utilizar la api es por medio de endpoints definidos en la carpeta de routes.

Ejemplo:

```bash
"use strict";

var express = require("express");
var { ensureAuth } = require("../middlewares/authenticated");
var Controller = require("../controllers/documentos");

var api = express.Router();

api.get("/documentos/",  Controller.get);
api.get("/documento-file/:idFile/:idDocumento?/:usuario?/:nombreDocumento?", ensureAuth, Controller.getFileById);
api.get("/documento-file-clasificacion/:idFile/:idDocumento/:usuario/:nombreDocumento", ensureAuth, Controller.getFileByIdClasificacion);
api.get("/documentos/:id/:usuario?", ensureAuth, Controller.getById);
api.get("/documentos-filtro/:filtro?", ensureAuth, Controller.getFiltrado);
api.post("/documentos/", ensureAuth, Controller.post)
api.delete("/documentos/:id/:usuario", ensureAuth, Controller.del)
api.put("/documentos/:id", ensureAuth, Controller.put)
api.put("/documentos-sinVersion/:id", ensureAuth, Controller.putSinVersion)
api.put("/documentos-borrar/:id", ensureAuth, Controller.borrar)
api.post("/upload/", ensureAuth, Controller.postUpload)
api.post("/uploadImg/", ensureAuth, Controller.postImg)
api.post("/documentos-text/", ensureAuth, Controller.getOcrGraphql)
module.exports = api;
```

Por ejemplo, al hacer un get/post/put/delete sobre un endpoint este envía la petición al controlador.

Ejemplo:

```bash
api.get("/documentos/",  Controller.get);
```

Se hace un envío al controlador controllers/home en la función getPrincipal.

```bash
async function get(req, res) {
    // Obtenemos la url
    const url = urls.urlDocumentos;
    // Realizamos peticion al servicio de strapi
    const resultado = await service.get(url + "?_limit=-1");
    // Retornamos el resultado
    res.status(resultado.status).send(resultado);
}
```

A su vez, la petición pasa del controlador al service services/strapi en la función get

```bash
async function get(url) {

    // Realizamos la peticion
    return axios({
            method: "GET",
            url: url,
            headers: {
                'content-type': 'application/json',
            },
            params: {}
        }).then(function(result) {
            // Si todo salio bien retornamos el resultado al controlador
            return {
                status: 200,
                error: '',
                data: result.data
            }
        })
        .catch(function(error) {
            // guardar LOG
            // retornamos el error al controlador
            return {
                status: 500,
                error: error,
                data: error
            }
        });

}
```

Esta funcionalidad es para obtener los datos desde strapi. En este proyecto no se utiliza base de datos, la fuente de datos es strapi.
