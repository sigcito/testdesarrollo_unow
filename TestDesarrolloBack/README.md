## Estructura del proyecto api-gestor-documental

El proyecto está desarrollado con nodejs (express), y está dividido de la siguiente manera:

-   routes (rutas)
-   controllers (controladores)
-   services (servicios)
-   models (modelo mapeados a bd)

## Instalación

Para iniciar el funcionamiento del proyecto api, hacemos lo siguiente:

1. Instalar la API

    ```
    cd api_cmc
    npm install
    ```

2. Configurar las variables de entorno en el archivo .env

    Ejemplo:

    ```
    urlCms = 'http://10.20.30.7:1338'
    port = 3500
    usuarioStrapi = 'hebernevarez@gmail.com' 
    passwordStrapi = 'M@aster123'             
    ```

3. Iniciar el CMC

    ```
    npm start
    ```
   Para mas detalles del funcionamiento de este proyecto consultar [API_README](./API_README.md)
