'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const os = require("os");
const formData = require("express-form-data");
const jsdom = require("jsdom");
const morgan = require('morgan');
const { JSDOM } = jsdom;

// agregar archivos para las tareas programadas


// Banxico

var app = express('200mb');

const options = {
    uploadDir: os.tmpdir(),
    autoClean: true,
    maxFieldsSize: '100mb'
};

//var pdf = require('./routes/pdf');
var usuariosRouter = require('./routes/usuarios');

app.use(bodyParser.urlencoded({  extended: true,
  limit: '50mb',
  parameterLimit: 100000
  }))
 app.use(bodyParser.json({
  limit: '50mb',
  parameterLimit: 100000
 }))

//ver peticiones web
app.use(morgan('dev'));

// parse data with connect-multiparty. 
app.use(formData.parse(options));
// delete from the request all empty files (size == 0)
app.use(formData.format());
// change the file objects to fs.ReadStream 
app.use(formData.stream());
// union the body and the files
app.use(formData.union());

// configurar cabeceras http
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Request-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE');
    res.header('Allow', 'GET, POST, PUT, OPTIONS, DELETE');
    next();
});

// rutas base
//app.use("/api", conekta);
app.use('/', usuariosRouter);


console.log("Corriendo");
module.exports = app;