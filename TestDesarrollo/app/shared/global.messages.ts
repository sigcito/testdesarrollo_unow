'use strict';
export var MessageType = {
    Success: 'Success',
    Fail: 'Danger'
}

export var Messages = {
    Success: "Guardado exitosamente.",
    Fail: "Registro no guardado.",
    Exists: "Registro ya existe.",
    Deleted:'Eliminado exitosamente.'
    
}