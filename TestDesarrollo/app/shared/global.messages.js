'use strict';
exports.MessageType = {
    Success: 'Success',
    Fail: 'Danger'
};
exports.Messages = {
    Success: "Guardado exitosamente.",
    Fail: "Registro no guardado.",
    Exists: "Registro ya existe.",
    Deleted: 'Eliminado exitosamente.'
};
//# sourceMappingURL=global.messages.js.map