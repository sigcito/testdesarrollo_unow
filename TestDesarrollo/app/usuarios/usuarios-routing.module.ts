import {NgModule}     from '@angular/core';
import {RouterModule} from '@angular/router';

import {UsuariosComponent} from './components/usuarios.component';
import {UsuariosListComponent} from './components/usuarios-list.component';
import {UsuariosFormComponent} from './components/usuarios-form.component';


@NgModule({
    imports: [
        RouterModule.forChild([
            {
                  
            path: '',
            component: UsuariosComponent,
            children: [
                {
                    path: '',
                    component: UsuariosListComponent
                },
                {
                    path: ':id',
                    component: UsuariosFormComponent
                }
            ]

            }
        ])
    ],
exports: [
    RouterModule
]
})

export class UsuariosRoutingModule {

}