import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'

import {UsuariosComponent} from './components/usuarios.component';
import {UsuariosListComponent} from './components/usuarios-list.component';
import {UsuariosFormComponent} from './components/usuarios-form.component';

import {UsuariosRoutingModule} from './usuarios-routing.module';

import { NotificationsComponents } from '../shared/ToastNotification/notification.component';
import {ModalPopupComponent  } from '../usable-component/modal-popup/modalpopup.component';

@NgModule({
    imports: [
        CommonModule,
        UsuariosRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        UsuariosListComponent,
        UsuariosComponent,
        UsuariosFormComponent,
        NotificationsComponents,
        ModalPopupComponent,
    ]
})

export class UsuariosModule {

}