"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var usuarios_service_1 = require('../services/usuarios.service');
var usuario_model_1 = require('../models/usuario.model');
var sharedata_service_1 = require('../services/sharedata.service');
var globalMessage = require('../../shared/global.messages');
var notification_service_1 = require('../../shared/ToastNotification/notification.service');
var notification_1 = require('../../shared/ToastNotification/notification');
var UsuariosListComponent = (function () {
    function UsuariosListComponent(_service, _notify, _sharedService, _router) {
        this._service = _service;
        this._notify = _notify;
        this._sharedService = _sharedService;
        this._router = _router;
        this.objDeleteUsuario = new usuario_model_1.Users();
    }
    UsuariosListComponent.prototype.ngOnInit = function () {
        if (this._sharedService.dataPassed) {
            this._notify.add(new notification_1.Notification(this._sharedService.dataPassed.type, this._sharedService.dataPassed.message));
        }
        this.bindGrid();
    };
    UsuariosListComponent.prototype.bindGrid = function () {
        var _this = this;
        this._service.getUsuarios()
            .subscribe(function (res) {
            _this.usuarios = res;
            _this.usuarios.forEach(function (element) {
                element['Fecha_nac'] = element.Fecha_nac.split('T')[0];
            });
            console.log(_this.usuarios);
        });
    };
    UsuariosListComponent.prototype.confirmDelete = function () {
        this.showDialog = false; /// Close dialog
        this.deleteUsuarios(this.objDeleteUsuario);
    };
    UsuariosListComponent.prototype.deleteUsuarios = function (usuarios) {
        var _this = this;
        var flag = 0;
        this._service.insertUpdateDeleteUsuarios(usuarios, true)
            .subscribe(function (data) {
            if (data) {
                _this._notify.add(new notification_1.Notification(globalMessage.MessageType.Success, globalMessage.Messages.Deleted));
                _this.bindGrid();
            }
        });
    };
    UsuariosListComponent.prototype.ngOnDestroy = function () {
        this._sharedService.dataPassed = null;
        this.objDeleteUsuario = null;
    };
    UsuariosListComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/usuarios/components/usuarios-list.component.html',
            providers: [usuarios_service_1.UsuariosService, notification_service_1.NotificationService],
        }), 
        __metadata('design:paramtypes', [usuarios_service_1.UsuariosService, notification_service_1.NotificationService, sharedata_service_1.ShareDataService, router_1.Router])
    ], UsuariosListComponent);
    return UsuariosListComponent;
}());
exports.UsuariosListComponent = UsuariosListComponent;
//# sourceMappingURL=usuarios-list.component.js.map