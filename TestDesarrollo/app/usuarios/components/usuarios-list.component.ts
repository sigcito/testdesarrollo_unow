import {Component, OnInit, OnDestroy} from '@angular/core';
import {RouterLink, Router} from '@angular/router';
import {NgFor} from '@angular/common';


import { UsuariosService } from '../services/usuarios.service'; 
import {Users} from '../models/usuario.model'

import { ShareDataService } from '../services/sharedata.service'; 
import globalMessage = require('../../shared/global.messages')


import {NotificationService} from '../../shared/ToastNotification/notification.service'
import { Notification } from '../../shared/ToastNotification/notification';


@Component({
    templateUrl: 'app/usuarios/components/usuarios-list.component.html',
    providers: [UsuariosService, NotificationService],

})

export class UsuariosListComponent implements OnInit, OnDestroy {
    usuarios: any[];
    showDialog: boolean;
    public visible: boolean;
    constructor(private _service: UsuariosService,
        private _notify: NotificationService
        , private _sharedService: ShareDataService,
        private _router: Router) {
    }

    objDeleteUsuario = new Users();

    ngOnInit() {
        if (this._sharedService.dataPassed) {
            this._notify.add(new Notification(this._sharedService.dataPassed.type, this._sharedService.dataPassed.message));
        }
        this.bindGrid()
    }

    bindGrid() { 
        this._service.getUsuarios()
        .subscribe(res => {
            this.usuarios = res;
            this.usuarios.forEach(element => {
                element['Fecha_nac'] = element.Fecha_nac.split('T')[0];
            })
            console.log(this.usuarios);
        })
    }

    confirmDelete() {
        this.showDialog = false; /// Close dialog
        this.deleteUsuarios(this.objDeleteUsuario);
    }

    deleteUsuarios(usuarios) {
        var flag = 0;
        this._service.insertUpdateDeleteUsuarios(usuarios, true)
            .subscribe(data => {
                if (data) {
                    this._notify.add(new Notification(globalMessage.MessageType.Success, globalMessage.Messages.Deleted));
                    this.bindGrid(); 
                }
            })
    }

    ngOnDestroy() {/// Clear Memory
        this._sharedService.dataPassed = null;
        this.objDeleteUsuario = null;
    }
}