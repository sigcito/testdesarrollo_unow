"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var usuarios_service_1 = require('../services/usuarios.service');
var usuario_model_1 = require('../models/usuario.model');
var sharedata_service_1 = require('../services/sharedata.service');
var basicValidators_1 = require('../../shared/CustomValidators/basicValidators');
var notification_service_1 = require('../../shared/ToastNotification/notification.service');
var globalMessage = require('../../shared/global.messages');
var UsuariosFormComponent = (function () {
    function UsuariosFormComponent(fb, route, router, _notify, _service, _sharedService) {
        this.fb = fb;
        this.route = route;
        this.router = router;
        this._notify = _notify;
        this._service = _service;
        this._sharedService = _sharedService;
        this.events = [];
        this.puestos = [];
        this.usuario = new usuario_model_1.Users();
        this.userForm = this.fb.group({
            id: [],
            Nombre: ['', [forms_1.Validators.minLength(3), basicValidators_1.BasicValidators.required]],
            Apellido: ['', [forms_1.Validators.minLength(3), basicValidators_1.BasicValidators.required]],
            Puesto: ['', forms_1.Validators.required],
            Fecha_nac: [],
        });
    }
    UsuariosFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getPuestos();
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = params["id"];
            console.log(_this.id);
        });
        if (this.id > 0) {
            this.title = "Editar usuario";
        }
        else {
            this.title = "Añadir usuario";
        }
        if (!this.id) {
            return;
        }
        this._service.getUsuarios(this.id)
            .subscribe(function (res) {
            _this.usuario = res;
            _this.usuario['Fecha_nac'] = res.Fecha_nac.split('T')[0];
            var Form = (_this.userForm);
            if (_this.id > 0) {
                _this.userForm.setValue(res, { onlySelf: false });
            }
        });
    };
    UsuariosFormComponent.prototype.save = function (model, isValid) {
        var _this = this;
        var result;
        console.log(model);
        this._service.insertUpdateDeleteUsuarios(model)
            .subscribe(function (data) {
            if (data) {
                _this._sharedService.SetData(globalMessage.MessageType.Success, globalMessage.Messages.Success);
                _this.router.navigate(["usuarios"]);
            }
        });
    };
    UsuariosFormComponent.prototype.routerCanDeactivate = function (next, previous) {
        return confirm("¿Estas seguro?");
    };
    UsuariosFormComponent.prototype.getPuestos = function () {
        var _this = this;
        this._service.getPuestos()
            .subscribe(function (res) {
            console.log(res);
            res.positions.forEach(function (element) {
                _this.puestos.push({ "puesto": element });
            });
            console.log(_this.puestos);
        });
    };
    UsuariosFormComponent = __decorate([
        core_1.Component({
            templateUrl: 'app/usuarios/components/usuarios-form.component.html',
            providers: [usuarios_service_1.UsuariosService, basicValidators_1.BasicValidators, notification_service_1.NotificationService]
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, notification_service_1.NotificationService, usuarios_service_1.UsuariosService, sharedata_service_1.ShareDataService])
    ], UsuariosFormComponent);
    return UsuariosFormComponent;
}());
exports.UsuariosFormComponent = UsuariosFormComponent;
//# sourceMappingURL=usuarios-form.component.js.map