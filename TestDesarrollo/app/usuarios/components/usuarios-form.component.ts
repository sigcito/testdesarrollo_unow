import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule, FormsModule} from '@angular/forms';
import {ActivatedRoute, Router, CanDeactivate } from '@angular/router'

import {UsuariosService} from '../services/usuarios.service'
import {Users} from '../models/usuario.model'

import {ShareDataService} from '../services/sharedata.service';
import {BasicValidators} from '../../shared/CustomValidators/basicValidators';
import {NotificationService} from '../../shared/ToastNotification/notification.service';
import { Notification } from '../../shared/ToastNotification/notification';

import globalMessage = require('../../shared/global.messages');

@Component({
    templateUrl: 'app/usuarios/components/usuarios-form.component.html',
    providers: [UsuariosService, BasicValidators, NotificationService]
})

export class UsuariosFormComponent implements OnInit {
    public userForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];
    public id: Number;
    public title: string;
    public puestos: any[] = [];

    usuario = new Users(); 

    constructor(
        private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private _notify: NotificationService,
        private _service: UsuariosService,
        private _sharedService: ShareDataService
    ) {
        this.userForm = this.fb.group({ 
            id: [],
            Nombre: ['', [<any>Validators.minLength(3), BasicValidators.required]],
            Apellido: ['', [<any>Validators.minLength(3), BasicValidators.required]],
            Puesto: ['', Validators.required],
            Fecha_nac: [],
        })
    }

    private sub: any;

    ngOnInit() {
        this.getPuestos();

        this.sub = this.route.params.subscribe(params => {
            this.id = params["id"];
            console.log(this.id);
        })

        if (this.id > 0) { 
            this.title = "Editar usuario"
        } else {
            this.title = "Añadir usuario"
        }

        if (!this.id) {
            return;
        }

        this._service.getUsuarios(this.id)
            .subscribe(res => {
                this.usuario = res;
                this.usuario['Fecha_nac'] = res.Fecha_nac.split('T')[0];
                let Form = (this.userForm);
                if (this.id > 0) { 
                    (<FormGroup>this.userForm).setValue(res, { onlySelf: false });
                }
            }
            )
    }

    save(model: Users, isValid: boolean) {
        var result;
        console.log(model);

        this._service.insertUpdateDeleteUsuarios(model)
            .subscribe(data => {
                if (data) {
                    this._sharedService.SetData(globalMessage.MessageType.Success, globalMessage.Messages.Success);
                    this.router.navigate(["usuarios"]);
                }
            }
            )
    }

    routerCanDeactivate(next, previous) {
        return confirm("¿Estas seguro?");
    }

    getPuestos(){
        this._service.getPuestos()
        .subscribe(res => {
            console.log(res);
            res.positions.forEach(element => {
                this.puestos.push({"puesto": element});
            });
            console.log(this.puestos);
        })
    }
}

