import {Component, OnInit} from '@angular/core';
import { ShareDataService } from '../services/sharedata.service'; 

@Component({
    template: `
      <router-outlet></router-outlet>
    `,
    providers: [ShareDataService]
})

export class UsuariosComponent {
    title = "Usuarios";
}

