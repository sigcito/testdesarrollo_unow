import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {Users} from '../models/usuario.model'

import globalVariables = require('../../shared/global.variables')

@Injectable()
export class UsuariosService {

    private apiUrl = globalVariables.apiUrl;
    private webUrl = globalVariables.webUrl + "/";
    private puestosUrl = 'https://ibillboard.com/api/positions';

    constructor(private _http: Http) {
    }

    getUsuarios(userId?) {
        if (userId > 0) {
            return this._http.get(this.apiUrl + "usuarios/" + userId)
                .map(res => res.json()[0])
            //             .map(this.extractData)
        }
        else {
            return this._http.get(this.apiUrl + 'usuarios')
                .map(res => res.json())
        }
    }

    private extractData(res: Response) {
        let body = res.json();
        return body[0];
    }

    insertUpdateDeleteUsuarios(usuario, isDetele = false) {
        var usuariosModel = new Users();

        let body = usuario;
        console.log(body);
        let headers = new Headers({ 'Content-Type': 'application/json', async: false });
        let options = new RequestOptions({ headers: headers });
        if(body.id){
            if (isDetele) {
                return this._http.delete(this.apiUrl + "usuarios/"+body.id,  body)
                .map(res => {
                    // If request fails, throw an Error that will be caught
                    if (res.status < 200 || res.status >= 300) {
                        throw new Error('This request has failed ' + res.status);
                    }
                    // If everything went fine, return the response
                    else {
                        return res.json();
                    }
                })
            }else{
            return this._http.put(this.apiUrl + "usuarios/"+body.id,  body,  options)
            .map(res => {
                // If request fails, throw an Error that will be caught
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('This request has failed ' + res.status);
                }
                // If everything went fine, return the response
                else {
                    return res.json();
                }
            })
        }
        }else{
            return this._http.post(this.apiUrl + "usuarios/",  body,  options)
            .map(res => {
                // If request fails, throw an Error that will be caught
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('This request has failed ' + res.status);
                }
                // If everything went fine, return the response
                else {
                    return res.json();
                }
            })
        }
        // .subscribe(
        // (data) => this.data = data, // Reach here if res.status >= 200 && <= 299
        // (err) => this.error = err);
        // .catch(this.handleError);


    }

    getPuestos() {
        return this._http.get(this.puestosUrl)
        .map(res => res.json())
    }
    private handleError(error: Response) {
        console.error(error);
    }
}
