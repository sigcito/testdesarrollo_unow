"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var usuario_model_1 = require('../models/usuario.model');
var globalVariables = require('../../shared/global.variables');
var UsuariosService = (function () {
    function UsuariosService(_http) {
        this._http = _http;
        this.apiUrl = globalVariables.apiUrl;
        this.webUrl = globalVariables.webUrl + "/";
        this.puestosUrl = 'https://ibillboard.com/api/positions';
    }
    UsuariosService.prototype.getUsuarios = function (userId) {
        if (userId > 0) {
            return this._http.get(this.apiUrl + "usuarios/" + userId)
                .map(function (res) { return res.json()[0]; });
        }
        else {
            return this._http.get(this.apiUrl + 'usuarios')
                .map(function (res) { return res.json(); });
        }
    };
    UsuariosService.prototype.extractData = function (res) {
        var body = res.json();
        return body[0];
    };
    UsuariosService.prototype.insertUpdateDeleteUsuarios = function (usuario, isDetele) {
        if (isDetele === void 0) { isDetele = false; }
        var usuariosModel = new usuario_model_1.Users();
        var body = usuario;
        console.log(body);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json', async: false });
        var options = new http_1.RequestOptions({ headers: headers });
        if (body.id) {
            if (isDetele) {
                return this._http.delete(this.apiUrl + "usuarios/" + body.id, body)
                    .map(function (res) {
                    // If request fails, throw an Error that will be caught
                    if (res.status < 200 || res.status >= 300) {
                        throw new Error('This request has failed ' + res.status);
                    }
                    else {
                        return res.json();
                    }
                });
            }
            else {
                return this._http.put(this.apiUrl + "usuarios/" + body.id, body, options)
                    .map(function (res) {
                    // If request fails, throw an Error that will be caught
                    if (res.status < 200 || res.status >= 300) {
                        throw new Error('This request has failed ' + res.status);
                    }
                    else {
                        return res.json();
                    }
                });
            }
        }
        else {
            return this._http.post(this.apiUrl + "usuarios/", body, options)
                .map(function (res) {
                // If request fails, throw an Error that will be caught
                if (res.status < 200 || res.status >= 300) {
                    throw new Error('This request has failed ' + res.status);
                }
                else {
                    return res.json();
                }
            });
        }
        // .subscribe(
        // (data) => this.data = data, // Reach here if res.status >= 200 && <= 299
        // (err) => this.error = err);
        // .catch(this.handleError);
    };
    UsuariosService.prototype.getPuestos = function () {
        return this._http.get(this.puestosUrl)
            .map(function (res) { return res.json(); });
    };
    UsuariosService.prototype.handleError = function (error) {
        console.error(error);
    };
    UsuariosService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], UsuariosService);
    return UsuariosService;
}());
exports.UsuariosService = UsuariosService;
//# sourceMappingURL=usuarios.service.js.map